package ua.nure.garashchenko;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/handler")
public class Handler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Handler() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String country = request.getParameter("country");
		String gender = request.getParameter("gender");
		String result = "не опрелены";
		boolean isFemale = gender.equals("female");
		switch(country) {
		case "1":
			result = "украинец";
			if (isFemale) {
				result = "украинка";
			}
			break;
		case "2":
			result = "россиянин";
			if (isFemale) {
				result = "россиянка";
			}
			break;
		case "3":
			result = "немец";
			if (isFemale) {
				result = "немка";
			}	
			break;
		}
		request.getSession().setAttribute("strResult", result);
		response.sendRedirect("./answer.jsp");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
